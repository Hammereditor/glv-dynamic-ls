<?php
	include "utils.php";
	header('Content-Type: application/json');
	$dbConn = createDBconnection();
	
	if ($dbConn->connect_errno)
	{
		printf("Error while connecting to database: %s\n", $dbConn->connect_error);
		exit();
	}
	
	$crResult;
	if (!$crResult = $dbConn->query("SELECT * FROM languages;"))
	{
		echo "Error while querying database";
		return;
	}
	
	$jsonStr = "[";
	while($row = $crResult->fetch_assoc())
	{
		$jsonStr .= "{\"id\": " . $row["LanguageID"] . ", \"englishName\": \"" . $row["EnglishName"] . "\", \"inLanguageName\": \"" . $row["InLanguageName"] . "\", \"iconURL\": \"" . $row["IconURL"] . "\"}, ";
	}
	
	$jsonStr = substr($jsonStr, 0, strlen($jsonStr) - 2);
	$jsonStr .= "]";
	echo $jsonStr;
?>
