USE gangalib;

DROP TABLE IF EXISTS Patents;
CREATE TABLE Patents (
  LaureateID INTEGER(32) UNSIGNED NOT NULL REFERENCES Laureates(LaureateID),
  Title VARCHAR(100),
  PublicationSerialNumber VARCHAR(100),
  PublicationType VARCHAR(40),
  PublicationDate DATE, 
  FilingDate DATE, 
  Inventors VARCHAR(100),
  Abstract VARCHAR(300),
  RepresentativeFigures VARCHAR(100)
);

DROP TABLE IF EXISTS Books;
CREATE TABLE Books (
  LaureateID INTEGER(32) UNSIGNED NOT NULL REFERENCES Laureates(LaureateID),
  Title VARCHAR(150),
  Volume VARCHAR(30),
  Contributor VARCHAR(150),
  Editor VARCHAR(50),
  Translator VARCHAR(100),
  Publisher VARCHAR(100),
  PublishYear INTEGER(16),
  Edition VARCHAR(30),
  Language VARCHAR(20),
  ISBN VARCHAR(100),
  Length INTEGER(16) UNSIGNED,
  Subjects VARCHAR(1000),
  CoverImage VARCHAR(100),
  BookLink VARCHAR(100),
  Summary TEXT 
);
