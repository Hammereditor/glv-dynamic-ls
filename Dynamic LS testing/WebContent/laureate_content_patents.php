<?php
	//get all information from DB
	$laureateID = $_GET["laureateID"];
	include "utils.php";
	$dbConn = createDBconnection();
	
	if (!isset($_GET["lang"]))
		$languageID = 1; //english
	else
		$languageID = $_GET["lang"];
	
	if ($dbConn->connect_errno)
	{
		printf("Error while connecting to database: %s\n", $dbConn->connect_error);
		exit();
	}
	
	$crResult;
	if (!$crResult = $dbConn->query("SELECT * FROM patents WHERE LaureateID = " . $laureateID . ";"))
	{
		echo "Error while querying database";
		return;
	}
?>

<style type="text/css">
    .titleField
    {
        background: #ACD6FF;
    }
    .blueField
    {
        background: #ECF5FF;
    }
    .leftcol
    {
        width: 170px;
    }
    .rightcol
    {
        width: 320px;
    }
</style>

<p class="heading center" style="font-size:24px">Patents</p>
<p>

<?php
	while($row = $crResult->fetch_assoc())
	{
		$publicationSerialNumber = $row["PublicationSerialNumber"];
		$title = $row["Title"];
        $publicationType = $row["PublicationType"];
        $publicationDate = $row["PublicationDate"];
        $filingDate = $row["FilingDate"];
        $inventors = $row["Inventors"];
        $abstract = $row["Abstract"];
        $figures = $row["RepresentativeFigures"];
		
		print("<table><tbody>");
		print("<tr><td class=\"center titleField\" colspan=\"2\"><b><em>" . $title . "</em></b></td></tr>");
		print("<tr class=\"blueField\"><td class=\"leftcol\">Publication No:</td>" . "<td class=\"rightcol\">" . $publicationSerialNumber . "</td></tr>");
		print("<tr><td class=\"leftcol\">Publication Type:</td>" . "<td class=\"rightcol\">" . $publicationType . "</td></tr>");
		print("<tr class=\"blueField\"><td class=\"leftcol\">Publication Date:</td>" . "<td class=\"rightcol\">" . $publicationDate . "</td></trr>");
		print("<tr><td class=\"leftcol\">Filing Date:</td>" . "<td class=\"rightcol\">" . $filingDate . "</td></tr>");
		print("<tr class=\"blueField\"><td class=\"leftcol\">Inventors:</td>" . "<td class=\"rightcol\">" . $inventors . "</td></tr>");
		print("<tr><td class=\"leftcol\">Abstract:</td>" . "<td class=\"rightcol\">" . $abstract . "</td></tr>");
        
        print("<tr class=\"blueField\"><td class=\"leftcol\">Representative Figure:</td>");
        if ($figures !== null)
            print("</tr><tr><td colspan=\"2\"><img src=\"images/{$figures}\" width=\"500\"></td></tr>");
        else
            print("<td class=\"rightcol\">No Figure</td></tr>");
        
        print("</tbody></table>");
        print("<br/><br/><br/>");
	}
	
	$crResult->close();
?>
</p>
