<?php
	//get all information from DB
	$laureateID = $_GET["laureateID"];
	include "utils.php";
	$dbConn = createDBconnection();
	
	if (!isset($_GET["lang"]))
		$languageID = 1; //english
	else
		$languageID = $_GET["lang"];
	
	if ($dbConn->connect_errno)
	{
		printf("Error while connecting to database: %s\n", $dbConn->connect_error);
		exit();
	}
	
	$crResult;
	if (!$crResult = $dbConn->query("SELECT * FROM Books WHERE LaureateID = " . $laureateID . ";"))
	{
		echo "Error while querying database";
		return;
	}
?>

<style type="text/css">
    .bookImage
    {
        width: 100px;
        border: 1px solid black;
    }
    .titleField
    {
        height: 140px;
        line-height: 140px;
        vertical-align: middle;
    }
    .titleSpan
    {
        display: inline-block;
        line-height: 25px;
    }
    .blueBar
    {
        background: #ECF5FF;
        height: 5px;
    }
    .blueField
    {
        background: #ECF5FF;
    }
    .leftcol
    {
        width: 170px;
    }
    .rightcol
    {
        width: 320px;
    }
</style>

<p class="heading center" style="font-size:24px">Books</p>
<p>
<?php
	while($row = $crResult->fetch_assoc())
	{
		$title = $row["Title"];
        $contributor = $row["Contributor"];
        $editor = $row["Editor"];
        $translator = $row["Translator"];
        $publisher = $row["Publisher"];
        $publishYear = $row["PublishYear"];
        $edition = $row["Edition"];
        $language = $row["Language"];
        $ISBN = $row["ISBN"];
        $length = $row["Length"];
        $subjects = $row["Subjects"];
        $coverImage = $row["CoverImage"];
        $bookLink = $row["BookLink"];
        $summary = $row["Summary"];
        
        if ($coverImage == null) {
            $coverImage = "no_cover_thumb.gif";
        }
        
        if ($length !== null) {
            $length .= " Pages";
        }
        
		print("<table style=\"width:500px;\"><tbody>");
        print("<tr><td colspan=\"2\" class=\"blueBar\"></td></tr>");
        print("<tr><td class=\"leftcol\"><img src=\"images/$coverImage\" class=\"bookImage\"></td>");
        print("<td class=\"rightcol\"><div class=\"titleField\"><span class=\"titleSpan\"><b>");
        if ($bookLink !== null) {
            print("<a href=\"$bookLink\" target=\"_blank\">$title</a>");
        } else {
            print($title);
        }
        print("</b></span></div></td></tr>");
        
		print("<tr class=\"blueField\"><td class=\"leftcol\">Contributor:</td>" . "<td class=\"rightcol\">" . $contributor . "</td></tr>");
		print("<tr><td class=\"leftcol\">Edition:</td>" . "<td class=\"rightcol\">" . $edition . "</td></tr>");
        if ($publisher !== null) {
            print("<tr class=\"blueField\"><td class=\"leftcol\">Publisher:</td>" . "<td class=\"rightcol\">" . $publisher . ", " . $publishYear . "</td></tr>");
        } else {
            print("<tr class=\"blueField\"><td class=\"leftcol\">Publish Year:</td>" . "<td class=\"rightcol\">" . $publishYear . "</td></tr>");
        }
        print("<tr><td class=\"leftcol\">ISBN:</td>" . "<td class=\"rightcol\">" . $ISBN . "</td></tr>");
		
        if ($language !== null) {
            print("<tr class=\"blueField\"><td class=\"leftcol\">Language:</td>" . "<td class=\"rightcol\">" . $language . "</td></tr>");
            print("<tr><td class=\"leftcol\">Translator:</td>" . "<td class=\"rightcol\">" . $translator . "</td></tr>");
        }
        
        print("<tr class=\"blueField\"><td class=\"leftcol\">Length:</td>" . "<td class=\"rightcol\">" . $length . "</td></tr>");
		print("<tr><td class=\"leftcol\">Subjects:</td>" . "<td class=\"rightcol\">" . $subjects . "</td></tr>");
        
        print("<tr class=\"blueField\"><td colspan=\"2\">Summary:</td></tr>");
        print("<tr class=\"blueField\"><td colspan=\"2\">$summary</td></tr>");
        print("</tbody></table>");
        print("<br/><br/><br/>");
	}
	
	$crResult->close();
?>
