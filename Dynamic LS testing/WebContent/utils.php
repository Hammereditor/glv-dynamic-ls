<?php
	function includeWithParams($urlWithParams)
	{
		echo file_get_contents('http://glv.hammereditor.net/' . dirname($_SERVER['SCRIPT_NAME']) . "/" . $urlWithParams);
	}

	//input: $_COOKIE["cookieTrackerJson"]
	function getLanguageFromCookie($cookie)
	{
		$languageID = 1;
		if (isset($cookie["cookieTrackerJson"]))
		{
			$json = json_decode($cookie["cookieTrackerJson"], true);
			$languageID = $json["languageID"];
		}
		
		return $languageID;
	}
	
	function getMultilingualTextForLaureate($laureateID, $languageID)
	{
		//get all the multilingual text for this laureate and language
		$dbConn = new mysqli("eu.hammereditor.net:3008", "glvtest", "b2c348a0", "mydb");
		if ($dbConn->connect_errno)
		{
			printf("Error while connecting to database: %s\n", $dbConn->connect_error);
			exit();
		}
		
		$langTextResult;
		if (!$langTextResult = $dbConn->query("SELECT * FROM multilingualtext WHERE LaureateID = " . $laureateID . " AND LanguageID = " . $languageID . ";"))
		{
			echo "Error while querying database";
			return;
		}
		
		//sort the multilingual texts into an array
		$langTextArray = [];
		while($row = $langTextResult->fetch_assoc())
		{
			$textID = $row["TextID"];
			$text = $row["Text"];
			$langTextArray[$textID] = $text;
		}
		
		$langTextResult->close();
		$dbConn->close();
		return $langTextArray;
	}
	
	function getMultilingualTextForNonLaureate($languageID)
	{
		//get all the multilingual text for this language
		$dbConn = new mysqli("eu.hammereditor.net:3008", "glvtest", "b2c348a0", "mydb");
		if ($dbConn->connect_errno)
		{
			printf("Error while connecting to database: %s\n", $dbConn->connect_error);
			exit();
		}
		
		$langTextResult;
		if (!$langTextResult = $dbConn->query("SELECT * FROM multilingualtext_nonlaureate WHERE LanguageID = " . $languageID . ";"))
		{
			echo "Error while querying database";
			return;
		}
		
		//sort the multilingual texts into an array
		$langTextArray = [];
		while($row = $langTextResult->fetch_assoc())
		{
			$textID = $row["TextID"];
			$text = $row["Text"];
			$langTextArray[$textID] = $text;
		}
		
		$langTextResult->close();
		$dbConn->close();
		return $langTextArray;
	}
	
	function getSupportedLanguages_laureate($laureateID, $textIDtoCheck)
	{
		$dbConn = new mysqli("eu.hammereditor.net:3008", "glvtest", "b2c348a0", "mydb");
		if ($dbConn->connect_errno)
		{
			printf("Error while connecting to database: %s\n", $dbConn->connect_error);
			exit();
		}
		
		$langTextResult;
		$subQuery = "SELECT LanguageID FROM multilingualtext WHERE LaureateID = " . $laureateID . " AND TextID = " . $textIDtoCheck . "";
		$mainQuery = "SELECT * FROM languages WHERE LanguageID IN (" . $subQuery . ");";
		
		if (!$langTextResult = $dbConn->query($mainQuery))
		{
			echo("Error while querying database. Query: " . $mainQuery);
			return;
		}
		
		$langAvailableArray = [];
		while($row = $langTextResult->fetch_assoc())
		{
			$langInfoArray = [];
			$langInfoArray["LanguageID"] = $row["LanguageID"];
			$langInfoArray["InLanguageName"] = $row["InLanguageName"];
			$langInfoArray["EnglishName"] = $row["EnglishName"];
			$langInfoArray["IconURL"] = $row["IconURL"];
			
			array_push($langAvailableArray, $langInfoArray);
		}
		
		$langTextResult->close();
		$dbConn->close();
		return $langAvailableArray;
	}
	
	function createDBconnection()
	{
		return new mysqli("glv.hammereditor.net:3008", "glvtest", "b2c348a0", "mydb");
	}
?>
