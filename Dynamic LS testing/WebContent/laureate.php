<?php
	include "utils.php";
	//get all information from DB
	$dbConn = createDBconnection();
	$laureateID = $_GET["laureateID"];
	$languageID = getLanguageFromCookie($_COOKIE);
	
	if ($dbConn->connect_errno)
	{
		printf("Error while connecting to database: %s\n", $dbConn->connect_error);
		exit();
	}
	
	$rowData_laureate;
	if ($laureateResult = $dbConn->query("SELECT * FROM laureates WHERE LaureateID = " . $laureateID . ";"))
	{
		for ($row_no = 0; $row_no < $laureateResult->num_rows; $row_no++)
		{
			$laureateResult->data_seek($row_no);
			$rowData_laureate = $laureateResult->fetch_assoc();
		}
		
		$laureateResult->close();
	}
	
	$rowData_award;
	if ($awardResult = $dbConn->query("SELECT * FROM awards WHERE AwardID = " . $rowData_laureate["AwardID"] . ";"))
	{
		for ($row_no = 0; $row_no < $awardResult->num_rows; $row_no++)
		{
			$awardResult->data_seek($row_no);
			$rowData_award = $awardResult->fetch_assoc();
		}
		
		$awardResult->close();
	}
	
	$infoPage = $_GET["infoPage"];
	$page_contentUrl = "laureate_content_" . $infoPage . ".php?laureateID=" . $laureateID . "&lang=" . $languageID;
	
	//variables to use on page
	$page_laureateName_last = $rowData_laureate["Name_Last"];
	$page_laureateName_middle = $rowData_laureate["Name_Middle"];
	$page_laureateName_first = $rowData_laureate["Name_First"];
	$page_laureateName = $page_laureateName_first . " " . $page_laureateName_middle . " " . $page_laureateName_last;
	
	$page_laureateDegrees = $rowData_laureate["Degrees"];
	$page_laureateInstitution = $rowData_laureate["Institution"];
	
	$page_laureateQBT;
		//look at the biography table to get the TextID
		$rowData_quickBiography;
		
		if ($quickBiographyResult = $dbConn->query("SELECT QuickBiographyText_TextID FROM biography WHERE LaureateID = " . $laureateID . ";"))
		{
			for ($row_no = 0; $row_no < $quickBiographyResult->num_rows; $row_no++)
			{
				$quickBiographyResult->data_seek($row_no);
				$rowData_quickBiography = $quickBiographyResult->fetch_assoc();
			}
			
			$quickBiographyResult->close();
		}
		else
		{
			echo "Error while querying database";
			return;
		}
		
		$langTextArray = getMultilingualTextForLaureate($laureateID, $languageID);
		
		$quickBiography_textID = $rowData_quickBiography["QuickBiographyText_TextID"];
		if (!array_key_exists($quickBiography_textID, $langTextArray))
			$page_laureateQBT = "Quick biography for this laureate not supported for this language";
		else
			$page_laureateQBT = $langTextArray[$quickBiography_textID];
	
	$page_laureatePhotoUrl = $rowData_laureate["PhotoUrl"];
	$page_laureatePhotoCredit = $rowData_laureate["PhotoCredit"];
	
	$page_laureateBirth_date = $rowData_laureate["Birth_Date"];
	$page_laureateBirth_city = $rowData_laureate["Birth_City"];
	$page_laureateBirth_state = $rowData_laureate["Birth_State"];
	$page_laureateBirth_country = $rowData_laureate["Birth_Country"];
	$page_laureateBirth_dateFormatted = date('d M Y', strtotime($page_laureateBirth_date));
	$page_laureateBirth_placeFormatted = $page_laureateBirth_city . ", " . $page_laureateBirth_state . ", " . $page_laureateBirth_country;
	
	$page_awardFullTitle = $rowData_award["FullTitle"];
	//echo getSupportedLanguages_laureate($laureateID, $quickBiography_textID)[0];
?>

<?php
	includeWithParams("frame_top.php?pageTitle=" . urlencode($page_laureateName));
?>

<div style="width:500px;float:left;font-size:16px;text-align:left">
<div style="width:500px;float:left">
	<h1 style="font-size:3em;margin-top:-20px"><?php echo $page_laureateName . " " . $page_laureateDegrees; ?></h1>
	<p style="margin-top:-30px"><strong><?php echo $page_awardFullTitle; ?></strong></p>
	<p class="profilebk">
		<em><?php echo $page_laureateQBT; ?></em>
	</p>

	<?php includeWithParams($page_contentUrl); ?>
	
	</p><div style="clear:both; min-height: 600px;"></div>
	<div id="Shape4" style="margin-left:-250px;clear:both">
		<div style="color:#000;font-size:12px;padding:15px;text-align:center">Ganga Library is a 501(c)(3) nonprofit organization, USA &nbsp;&nbsp;Copyright &copy; 2015 Ganga Library Inc.&nbsp;&nbsp; All Rights reserved.&nbsp;&nbsp;  E-Mail: gangalibrary@hotmail.com

	</div>
</div>
</div>
</div>

<div style="width:200px;float:left;padding-left:10px">
	<img src="<?php echo $page_laureatePhotoUrl; ?>" alt="Fama" width="200"><br>
	<span style="font-size:10px">Photo: <?php echo $page_laureatePhotoCredit; ?></span>
	<p style="text-align:left">
	<b>Name:</b> <?php echo $page_laureateName; ?> <br><br>
	<b>Born:</b> 
	<?php echo $page_laureateBirth_dateFormatted . " " . $page_laureateBirth_placeFormatted; ?>
	<br><br>
	<b>Institution:</b> <?php echo $page_laureateInstitution; ?> <br><br>
	
	<p>
	<!-- <a href="fama.php">Biography</a><br>
	<a href="famavita.php">Vita</a><br>
	<a href="famaarticals.php">Journal Articles</a><br>
	<a href="famacr.php">Copyrights</a><br>
	<a href="famabooks.php">Books</a><br>
	<a href="famaextres.php">External Resources Text/Videos</a>
	-->
	
	<?php
		$cpResult;
		if (!$cpResult = $dbConn->query("SELECT * FROM contentpages;"))
		{
			echo "Error while querying database";
			return;
		}
		
		while($row = $cpResult->fetch_assoc())
		{
			print("<a href=\"laureate.php?laureateID=" . $laureateID . "&infoPage=" . $row["InfoPageName"] . "\">" . $row["UIname"] . "</a><br>");
		}
		
		//print("<a href=\"laureate.php?laureateID=" . $laureateID . "&infoPage=vita\">Vita</a><br>");
		//print("<a href=\"laureate.php?laureateID=" . $laureateID . "&infoPage=copyrights\">Copyrights</a><br>");
		//print("<a href=\"laureate.php?laureateID=" . $laureateID . "&infoPage=books\">Books</a><br>");
		//print("<a href=\"laureate.php?laureateID=" . $laureateID . "&infoPage=extres\">External Resources</a><br>");
	?>
	</p>
	
</div>

<?php includeWithParams("frame_bottom_multilingual.php?laureateID=" . $laureateID . "&textIDtoCheck=" . $quickBiography_textID); ?>
