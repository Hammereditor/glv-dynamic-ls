<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>
<?php
	$pageTitle = $_GET["pageTitle"];
	echo($pageTitle); 
?>
</title>
<link rel="stylesheet" href="staticsite/css/gangalib.css">
<link rel="stylesheet" href="resources/ms-Dropdown-master/css/msdropdown/dd.css">

<script type="text/javascript" src="resources/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="resources/jquery.cookie.js"></script>
<script type="text/javascript" src="resources/scripts.js"></script>
<script type="text/javascript" src="resources/ms-Dropdown-master/js/msdropdown/jquery.dd.min.js"></script>

<style>
body p,h1
{
	font-family:calibri,sans-serif;
	font-size:18px;
}
.bottom_menu li a{font-size:12px}

</style>
</head>
<body>

<div id="container">



<div style="width:970px">
	<div style="float:left;height:150px;width:250px">
		<!-- <div id="wb_Text7_languageSelector" class="left_sidebar" style="padding-right: 0px;">
			<h4>Select language</h4>
			
			<select name="webmenu" id="wb_Text7_languageSelector_select" style="width: 200px;">
			</select>
		</div> -->
	</div>
	<div style="float:left;width:360px">
		<img src="http://www.gangalib.org/images/ganga_header.png" width="300" style="margin:30px 0px 10px 80px;float:left"></div>
		<div style="font-size:19px;color:rgb(12,113,249);width:20px;float:left;margin-top:40px;margin-left:20px">&reg;</div>
		<div style="float:left;margin-left:80px;margin-top:20px"><img src="staticsite/images/Nobel.png" width="65"><br>
		<span style="colorbody p,h1
{
	font-family:calibri,sans-serif;
	font-size:18px;
}
.bottom_menu li a{font-size:12px}

">Alfred Nobel</span></div>
	
	
	<div style="float:right;width:120px;margin-top:15px">
		<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=P9BXMXQ69S2BN" target="_blank"><img src="staticsite/images/paypal.png" width="120" style="margin-top:25px"></a><br>
	</div>
	<h2 style="float:left;margin:10px 0px;color:rgb(24,0,218);font-size:14pt;margin:0px">Expert Source for Nobel and Laureates Achievements Online</h2>
</div>
<div id="speaker" style="top:25px;left:455px"><a href="http://www.pronouncenames.com/search?name=ganga" target="_blank"><img src="staticsite/images/speaker.png" alt="speaker" width="20"></a></div>


<ul class="drop_menu" style="clear:both">

	<li class="firstmain"><a href="staticsite/index.php" target="_self">Home</a></li>
	<li><a href="#" target="_self">About&nbsp;Us</a>

		<ul>
			<li class="firstitem lastitem"><a href="staticsite/Team.php" target="_self">Team</a></li>
			<li class="firstitem lastitem"><a href="staticsite/libraryname.php">Library name Ganga</a></li>
		</ul>
	</li>
	<li><a href="#">Value to You!</a></li>
	
	<li><a href="staticsite/Alfred_Nobel.php" target="_self">Alfred&nbsp;Nobel</a></li>

	<li><a href="laureates.php">Nobel Laureates&nbsp;&nbsp;</a></li>
	

	<li><a href="staticsite/nmsti.php">Nat'l Medal Laureates</a></li>
	<li><a href="staticsite/essay_contest.php" target="_self">Essay Contest</a></li>
	<li><a href="staticsite/event.php" target="_self">Events</a></li>
	<li><a href="#">Contact Us</a></li>
	
</ul>

  

	
	
	
	<ul class="bottom_menu" style="clear:both">
	<!--<li><a href="#">Education</a></li>
	<li><a href='#'>Drug<br>Discoveries</a></li>-->
	<li><a href='#'>Volunteers<br>
		&nbsp;</a>
		<ul>
			<li><a href="staticsite/Volunteers.php">New Volunteers</a></li>
			<li><a href="staticsite/ourvolunteers.php">Some of our Volunteers</a></li>
			<li><a href="staticsite/volunteerexp.php">Volunteer Experience</a></li>
		</ul>
	</li>
	<li><a href='staticsite/usgovernment.php'>US<br>Government</a></li>
	<li><a href='http://www.washingtonpost.com/blogs/worldviews/wp/2013/10/15/the-amazing-history-of-the-nobel-prize-told-in-maps-and-charts/' target="_blank">Statistics<br>of Prize</a></li>
	<li><a href='staticsite/humor.php'>Humor/<br>Quotations</a></li>
	<li><a href='staticsite/books.php'>Books<br>Available</a></li>
	<li><a href='staticsite/videofilms.php'>Videos<br>/Films</a></li>
	<li><a href='staticsite/historyofdiscovery.php'>History of<br>Discovery</a></li>
	<li><a href='staticsite/honoringlaureate.php'>Bearing<br>Laureate's Name</a></li>
	<li><a href='staticsite/patents.php'>Patents</a></li>
	<li><a href='staticsite/copyrights.php'>Copyrights/<br>Economists</a></li>
	
	<!--<li><a href="#">Immigration<br>History</a></li>-->
	</ul>


	
	
<div id="wb_Shape1" style="float:Left;width:220px;height:46px;z-index:8;">
<div id="Shape1"></div></div><div id="Shape11" style="float:left;position:relative">
<span style="color:#FFF;font-size:21px;position:absolute;left:20px;top:10px"><strong><?php echo($GLOBALS["pageTitle"]); ?></strong></span></div>
<div id="Shape11"></div>
<div id="wb_Shape5" style="position:absolute;left:0px;width:220px;height:429px;z-index:9;">
<div id="Shape5"></div>
</div>
<div id="wb_Text7" style="position:absolute;left:238px;margin-top:20px;width:716px;height:38px;z-index:14;text-align:left;">
<!-- insert page content here -->

<!-- insert bottom frame here -->
