<?php
	//get all information from DB
	$laureateID = $_GET["laureateID"];
	include "utils.php";
	$dbConn = createDBconnection();
	
	if (!isset($_GET["lang"]))
		$languageID = 1; //english
	else
		$languageID = $_GET["lang"];
	
	if ($dbConn->connect_errno)
	{
		printf("Error while connecting to database: %s\n", $dbConn->connect_error);
		exit();
	}
	
	$vitaResult;
	if (!$vitaResult = $dbConn->query("SELECT * FROM vita WHERE LaureateID = " . $laureateID . ";"))
	{
		echo "Error while querying database";
		return;
	}
	
	$langTextArray_laureate = getMultilingualTextForLaureate($laureateID, $languageID);
	$langTextArray_nonLaureate = getMultilingualTextForNonLaureate($languageID);
	
	$langTextResult_nonLaureate;
	if (!$langTextResult_nonLaureate = $dbConn->query("SELECT * FROM multilingualtext_nonlaureate WHERE LanguageID = " . $languageID . ";"))
	{
		echo "Error while querying database";
		return;
	}
	
	$langTextArray_nonLaureate = [];
	while($row = $langTextResult_nonLaureate->fetch_assoc())
	{
		$textID = $row["TextID"];
		$text = $row["Text"];
		$langTextArray_nonLaureate[$textID] = $text;
	}
	
	//organize rows into vita array, which has variable categories
	$vitaArray = []; //the rows hold the vita types, and the columns hold the entries
	
	while($row = $vitaResult->fetch_assoc())
	{
		$type_textID = $row["Type_TextID"];
		$text_textID = $row["Text_TextID"];
		
		if (!(array_key_exists($type_textID, $langTextArray_nonLaureate) && array_key_exists($text_textID, $langTextArray_laureate)))
		{
			echo "Language not supported for this page";
			return;
		}
		
		$type = $langTextArray_nonLaureate[$type_textID];
		$text = $langTextArray_laureate[$text_textID];
	
		if (!array_key_exists($type, $vitaArray)) //this type does not exist yet
			$vitaArray[$type] = [];
		array_push($vitaArray[$type], $text);
	}
	
	$vitaResult->close();
	$vitaArrayTypeNames = array_keys($vitaArray);
?>

<p class="heading center" style="font-size:24px">Vita</p>
<p>

<?php
	//print out categories
	foreach ($vitaArrayTypeNames as $currVitaTypeKey)
		print("<a href=\"#vita_" . $currVitaTypeKey . "\">" . $currVitaTypeKey . "</a><br>");
?>
</p>

<?php
	//print out entries
	
	for ($t = 0; $t < count($vitaArray); $t++)
	{
		$currVitaTypeKey = $vitaArrayTypeNames[$t];
		$currVitaType = $vitaArray[$currVitaTypeKey];
		print("<p id=\"vita_" . $currVitaTypeKey . "\">" . "<b>" . $currVitaTypeKey . "</b>" . "</p>");
		
		for ($e = 0; $e < count($currVitaType); $e++)
		{
			$currVitaTypeEntry = $currVitaType[$e];
			print("<p>" . $currVitaTypeEntry . "</p>");
		}
	}
?>

