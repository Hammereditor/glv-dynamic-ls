<?php
	//get all information from DB
	include "utils.php";
	includeWithParams("frame_top.php?pageTitle=Laureates");
	$languageID = getLanguageFromCookie($_COOKIE);
	$dbConn = createDBconnection();
	
	if ($dbConn->connect_errno)
	{
		printf("Error while connecting to database: %s\n", $dbConn->connect_error);
		exit();
	}
	
	//get all the multilingual text for all laureates and this language
	$langTextResult;
	if (!$langTextResult = $dbConn->query("SELECT * FROM multilingualtext WHERE LanguageID = " . $languageID . ";"))
	{
		echo "Error while querying database";
		return;
	}
	
	//sort the multilingual texts into an array
	$langTextArray = [];
	
	while($row = $langTextResult->fetch_assoc())
	{
		$laureateID = $row["LaureateID"];
		$textID = $row["TextID"];
		$text = $row["Text"];
		$langTextArray[$laureateID][$textID] = $text;
	}
	
	$rowData_laureate;
	if ($laureateResult = $dbConn->query("SELECT * FROM laureates;"))
	{
		for ($row_no = 0; $row_no < $laureateResult->num_rows; $row_no++)
		{
			$laureateResult->data_seek($row_no);
			$rowData_laureate = $laureateResult->fetch_assoc();
			
			//get the award data for the laureate
			$rowData_award;
			
			if ($awardResult = $dbConn->query("SELECT * FROM awards WHERE AwardID = " . $rowData_laureate["AwardID"] . ";"))
			{
				for ($row_no2 = 0; $row_no2 < $awardResult->num_rows; $row_no2++)
				{
					$awardResult->data_seek($row_no2);
					$rowData_award = $awardResult->fetch_assoc();
				}
				
				$awardResult->close();
			}
			
			//organize the variables
			$laureateID = $rowData_laureate["LaureateID"];
			//$langTextArray = getMultilingualTextForLaureate($laureateID, $languageID);
			
			$page_laureateName_last = $rowData_laureate["Name_Last"];
			$page_laureateName_middle = $rowData_laureate["Name_Middle"];
			$page_laureateName_first = $rowData_laureate["Name_First"];
			$page_laureateName = $page_laureateName_first . " " . $page_laureateName_middle . " " . $page_laureateName_last;
			
			//get the quick biography text
			$rowData_quickBiography = null;
			
			if ($quickBiographyResult = $dbConn->query("SELECT QuickBiographyText_TextID FROM biography WHERE LaureateID = " . $laureateID . ";"))
			{
				for ($row_no3 = 0; $row_no3 < $quickBiographyResult->num_rows; $row_no3++)
				{
					$quickBiographyResult->data_seek($row_no3);
					$rowData_quickBiography = $quickBiographyResult->fetch_assoc();
				}
				$quickBiographyResult->close();
			}
			else
			{
				echo "Error while querying database";
				return;
			}
			
			$page_laureateQBT = "null";
			
			if ($rowData_quickBiography == null)
				$page_laureateQBT = "No quick biography text for this laureate<br><br><br><br><br>";
			else
			{
				$quickBiography_textID = $rowData_quickBiography["QuickBiographyText_TextID"];
				if (!isset($langTextArray[$laureateID][$quickBiography_textID]))
					$page_laureateQBT = "Quick biography for this laureate not supported for this language<br><br><br><br><br>";
				else
					$page_laureateQBT = $langTextArray[$laureateID][$quickBiography_textID];
			}
			
			$page_laureatePhotoUrl = $rowData_laureate["PhotoUrl"];
			$page_laureatePhotoCredit = $rowData_laureate["PhotoCredit"];
			
			//print the HTML
			print("<div>");
				print("<div class=\"left\" style=\"width: 125px;\">");
					print("<img src=\"" . $page_laureatePhotoUrl . "\" alt=\"" . $page_laureateName_last . "\" width=\"100\">");
					print("<p style=\"text-align:left;font-size:11px;margin-top:-3px\">Photo: " . $page_laureatePhotoCredit . "</p>");
				print("</div>");
				print("<div style=\"width:500px;float:left;\">");
					print("<h1 style=\"font-size:3em\"><a href=\"laureate.php?laureateID=" . $rowData_laureate["LaureateID"] . "&infoPage=biography\">" . $page_laureateName . "</a></h1>");
					print("<p style=\"margin-top:-30px\"><strong>" . $rowData_award["FullTitle"] . "</strong></p>");
					print("<p>" . $page_laureateQBT . "</p>");
				print("</div>");
			print("</div>");
		}
		
		$laureateResult->close();
	}
?>

<?php
	//include static laureates
	if (file_exists("staticlaureates.html"))
	{
		include "staticlaureates.html";
	}

?>

<?php include 'frame_bottom.php'; ?>
