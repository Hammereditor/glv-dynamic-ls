<?php
	//get all information from DB
	$laureateID = $_GET["laureateID"];
	include "utils.php";
	$dbConn = createDBconnection();
	
	if (!isset($_GET["lang"]))
		$languageID = 1; //english
	else
		$languageID = $_GET["lang"];
	
	if ($dbConn->connect_errno)
	{
		printf("Error while connecting to database: %s\n", $dbConn->connect_error);
		exit();
	}
	
	$langTextArray = getMultilingualTextForLaureate($laureateID, $languageID);
	
	$langTextResult_nonLaureate;
	if (!$langTextResult_nonLaureate = $dbConn->query("SELECT * FROM multilingualtext_nonlaureate WHERE LanguageID = " . $languageID . " AND TextID >= 8 AND TextID <= 17;"))
	{
		echo "Error while querying database";
		return;
	}
	
	//sort the multilingual texts into an array
	$langTextArray_nonLaureate = [];
	while($row = $langTextResult_nonLaureate->fetch_assoc())
	{
		$textID = $row["TextID"];
		$text = $row["Text"];
		$langTextArray_nonLaureate[$textID] = $text;
	}
	$langTextResult_nonLaureate->close();
	
	$crResult;
	if (!$crResult = $dbConn->query("SELECT * FROM copyrights WHERE LaureateID = " . $laureateID . ";"))
	{
		echo "Error while querying database";
		return;
	}
?>

<style type="text/css">
	.copyrightField
	{
		padding-right: 30px;
		padding-top: 3px;
		padding-bottom: 3px;
	}
</style>

<p class="heading center" style="font-size:24px">Copyrights</p>
<p>

<?php
	while($row = $crResult->fetch_assoc())
	{
		$summaryTitle = $row["SummaryTitle"];
		$regNumber = $row["RegistrationNumber"];
		$title = $row["Title"];
		$desc = $row["Description"];
		$owner = $row["CopyrightOwner"];
		$imprint = $row["Imprint"];
		$claimant = $row["Claimant"];
		$copyrightNotice = $row["CopyrightNotice"];
		$creationYear = $row["CreationYear"];
		$publicationDate = $row["PublicationDate"];
		$authorship = $row["ApplicationAuthorship"];
		$names = $row["Names"];
		
		//$names = str_replace("\n", "<br>", $names);
		
		print("<p class=\"center\"><b><em>" . $summaryTitle . "</em></b></p>");
		print("<span class=\"leftcol copyrightField\"><b>" . $langTextArray_nonLaureate[8] . ":</b></span>" . "<span class=\"rightcol\">Entry not Found</span><br>");
		print("<span class=\"leftcol copyrightField\"><b>" . $langTextArray_nonLaureate[9] . ":</b></span>" . "<span class=\"rightcol\">" . $regNumber . "</span><br>");
		print("<span class=\"leftcol copyrightField\"><b>" . $langTextArray_nonLaureate[10] . ":</b></span>" . "<span class=\"rightcol\">" . $title . "</span><br>");
		print("<span class=\"leftcol copyrightField\"><b>" . $langTextArray_nonLaureate[11] . ":</b></span>" . "<span class=\"rightcol\">" . $imprint . "</span><br>");
		print("<span class=\"leftcol copyrightField\"><b>" . $langTextArray_nonLaureate[12] . ":</b></span>" . "<span class=\"rightcol\">" . $desc . "</span><br>");
		print("<span class=\"leftcol copyrightField\"><b>" . $langTextArray_nonLaureate[13] . ":</b></span>" . "<span class=\"rightcol\">" . $claimant . "</span><br>");
		print("<span class=\"leftcol copyrightField\"><b>" . $langTextArray_nonLaureate[14] . ":</b></span>" . "<span class=\"rightcol\">" . $copyrightNotice . "</span><br>");
		print("<span class=\"leftcol copyrightField\"><b>" . $langTextArray_nonLaureate[15] . ":</b></span>" . "<span class=\"rightcol\">" . $creationYear . "</span><br>");
		print("<span class=\"leftcol copyrightField\"><b>" . $langTextArray_nonLaureate[16] . ":</b></span>" . "<span class=\"rightcol\">" . $authorship . "</span><br>");
		print("<span class=\"leftcol copyrightField\"><b>" . $langTextArray_nonLaureate[17] . ":</b></span>" . "<span class=\"rightcol\">" . $names . "</span><br>");
		print("<br><br><br>");
	}
	
	$crResult->close();
?>
</p>
