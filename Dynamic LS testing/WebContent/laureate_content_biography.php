<?php
	//get all information from DB
	$laureateID = $_GET["laureateID"];
	include "utils.php";
	$dbConn = createDBconnection();
	
	if (!isset($_GET["lang"]))
		$languageID = 1; //english
	else
		$languageID = $_GET["lang"];
	
	if ($dbConn->connect_errno)
	{
		printf("Error while connecting to database: %s\n", $dbConn->connect_error);
		exit();
	}
	
	//look at the biography table to get the TextID
	$rowData_biography;
	
	if ($biographyResult = $dbConn->query("SELECT BiographyText_TextID FROM biography WHERE LaureateID = " . $laureateID . ";"))
	{
		for ($row_no = 0; $row_no < $biographyResult->num_rows; $row_no++)
		{
			$biographyResult->data_seek($row_no);
			$rowData_biography = $biographyResult->fetch_assoc();
		}
		
		$biographyResult->close();
	}
	else
	{
		echo "Error while querying database";
		return;
	}
	
	$langTextArray = getMultilingualTextForLaureate($laureateID, $languageID);
	
	//variables to use on page
	$biography_textID = $rowData_biography["BiographyText_TextID"];
	if (!array_key_exists($biography_textID, $langTextArray))
	{
		echo "Language not supported for this page";
		return;
	}
	
	$page_biographyText = $langTextArray[$biography_textID];
?>

<p class="heading center" style="font-size:24px">Biography</p>
<?php
	echo $page_biographyText;
?>
