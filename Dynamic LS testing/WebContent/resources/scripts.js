////////////////////////////////////////////////////////////////////////////
// PERSISTENCE
////////////////////////////////////////////////////////////////////////////

function CookieTracker()
{
	//default values
	this.jsonObj = {
		"languageID": 1
	};
}

CookieTracker.prototype.getData = 
function()
{
	return this.jsonObj;
};

CookieTracker.prototype.saveData = 
function()
{
	$.cookie('cookieTrackerJson', JSON.stringify(this.jsonObj), {expires: 180, path: '/'});
};

CookieTracker.prototype.loadData = 
function()
{
	if ($.cookie("cookieTrackerJson") != undefined)
		this.jsonObj = JSON.parse($.cookie("cookieTrackerJson"));
	else
		this.saveData(); //saves the default configuration
};

var cookieT = new CookieTracker();
cookieT.loadData();

////////////////////////////////////////////////////////////////////////////
// LOCALES
////////////////////////////////////////////////////////////////////////////

//1: Create client-side HTML/JS UI for selecting languages
//2: Make client script store cookie for language
//3: Make sure server receives cookie
//4: Move language-table querying to one server script, instead of querying and processing the array in all laureate content scripts
//5: For that separate script, create function to process locale to use, based on cookie and URL
//6: Modify language table to include flag icon URL
//7: Dynamically generate this client-sided script
//8: Create table of content pages which holds filename and UIname
//9: Make side content-page links dynamic

var debug = true;
/*
function insertLanguageCode()
{
	var html = "";
	for (var i = 0; i < locales.length; i++)
	{
		//var id = locales[i].id;
		var englishName = locales[i].englishName;
		var inLanguageName = locales[i].inLanguageName;
		var iconURL = locales[i].iconURL;
		
		html += "<p><img src=\"" + iconURL + "\"/>" + englishName + " (" + inLanguageName + ")</p>";
	}
	
	$("#languageContainer").html($("#languageContainer").html() + html);
}
*/

/*var locales; = 
[
	{"id": 1, "englishName": "English", "inLanguageName": "English", "iconURL": "resources/flagicons/gb.png"},
	{"id": 2, "englishName": "Gibberish", "inLanguageName": "#f3ww0-3", "iconURL": "resources/flagicons/ly.png"}
];*/

//initialize language selector
/*$(document).ready(function(e)
{
	$.ajax(
	{
		type: "GET",
		url: "languages.php",
		async: true,
		cache: true,
		success: function(data)
		{
			try
			{
				if (debug)
					console.log(data);
				locales = data; //JSON.parse(data);
				initializeLanguageSelector();
			}
			catch (e) { 
				console.log("Error while getting languages: " + e);
			}
		},
		error: function(e) {
			console.log("Error while getting languages: " + e);
		}
	});
});

function initializeLanguageSelector()
{
	var languageSelectorEle = $("#wb_Text7_languageSelector_select");
	
	for (var i = 0; i < locales.length; i++) //append languages to selector
	{
		var optionHTML = "<option value=\"" + locales[i].id + "\" data-image=\"" + locales[i].iconURL + "\">" + locales[i].englishName + " (" + locales[i].inLanguageName + ")</option>";
		languageSelectorEle.html(languageSelectorEle.html() + optionHTML);
	}
	
	languageSelectorEle.msDropDown();
	//languageSelectorEle.val(cookieT.getData().languageID);
	$("#wb_Text7_languageSelector_select option[value=" + cookieT.getData().languageID + "]").prop('selected', 'selected').change();
	
	//function for setting language
	languageSelectorEle.on('change', function() {
		var originalLanguageID = cookieT.getData().languageID;
		if (debug)
			console.log("New language selected. original ID: " + originalLanguageID + ", new ID: " + languageSelectorEle.val());
		
		cookieT.getData().languageID = languageSelectorEle.val();
		cookieT.saveData();
		if (languageSelectorEle.val() != originalLanguageID) //language changed, so refresh page
			window.location.href = window.location.href;
	});
}*/

//////////////////////////////////////////////////////////////////
// NEW LANGUAGE SYSTEM
//////////////////////////////////////////////////////////////////

function testLocaleArray()
{
	console.log(locales);
}

function generateSidebarHTML()
{
	/*
	 * <div>
  <input type="radio" name="locale" value="1">
  	<img src="resources/flagicons/gb.png">
  <p style="display: inline;">English (English)</p>
</div><div>
  <input type="radio" name="locale" value="2">
  	<img src="resources/flagicons/ly.png">
  <p style="display: inline;">Latin (Neque)</p>
</div>
	 * 
	 */
	
	var html = "";
	
	for (var i = 0; i < locales.length; i++)
	{
		var id = locales[i]["id"];
		var inLanguageName = locales[i]["inLanguageName"];
		var englishName = locales[i]["englishName"];
		var iconURL = locales[i]["iconURL"];
		
		html += "<div>";
			html += "<input type=\"radio\" name=\"locale\" value=\"" + id + "\"/>";
			html += "<div style=\"display: inline; padding-right: 10px;\"></div>";
			html += "<img src=\"" + iconURL + "\"/>";
			html += "<div style=\"display: inline; padding-right: 10px;\"></div>";
			html += "<p style=\"display: inline;\">" + englishName + " (" + inLanguageName + ")</p>";
		html += "</div>";
	}
	
	var languageContainerEle = $("#languageContainer");
	languageContainerEle.html(html);
}

$(document).ready(function(e)
{
	generateSidebarHTML(); 
	
	$("input[name='locale']").on('change', function() {
		var originalLanguageID = cookieT.getData().languageID;
		if (debug)
			console.log("New language selected. original ID: " + originalLanguageID + ", new ID: " + $("input[name='locale']:checked").val());
		
		cookieT.getData().languageID =  $("input[name='locale']:checked").val();
		cookieT.saveData();
		if ( $("input[name='locale']:checked").val() != originalLanguageID) //language changed, so refresh page
			window.location.href = window.location.href;
	});
	
	//$("input[name='locale']").attr("checked", );
	$("input[name='locale'][value=" + cookieT.getData().languageID + "]").prop('checked', true);
});
