
</div>
<div id="wb_CssMenu2" style="position:absolute;width:220px;height:260px;z-index:16;background-color:#DDD; padding-left: 10px;">
<h4 class="left_sidebar">Languages</h4><br>
<br>

<?php
	include "utils.php";
	$laureateID = $_GET["laureateID"];
	$textIDtoCheck = $_GET["textIDtoCheck"];
	$supportedLanguages = getSupportedLanguages_laureate($laureateID, $textIDtoCheck);

	for ($l = 0; $l < count($supportedLanguages); $l++)
	{
		$langInfoArray = $supportedLanguages[$l];
		printf("<div>");
			printf("</img src=\"" . $langInfoArray["IconURL"] . "\">");
			printf("<p>" . $langInfoArray["EnglishName"] . " (" . $langInfoArray["InLanguageName"] . ")</p>");
		printf("</div>");
	}
?>
</div>
