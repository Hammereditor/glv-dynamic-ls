<?php
	//get all information from DB
	$laureateID = $_GET["laureateID"];
	include "utils.php";
	$dbConn = createDBconnection();
	
	if (!isset($_GET["lang"]))
		$languageID = 1; //english
	else
		$languageID = $_GET["lang"];
	
	if ($dbConn->connect_errno)
	{
		printf("Error while connecting to database: %s\n", $dbConn->connect_error);
		exit();
	}
	
	$crResult;
	if (!$crResult = $dbConn->query("SELECT * FROM photos WHERE LaureateID = " . $laureateID . ";"))
	{
		echo "Error while querying database: " . $crResult;
		return;
	}
	
	$langTextArray = getMultilingualTextForLaureate($laureateID, $languageID);
?>

<p class="heading center" style="font-size:24px">Photos</p>
<p>

<?php
	while($row = $crResult->fetch_assoc())
	{
		$photoUrl = $row["PhotoUrl"];
		$captionTextID = $row["Caption_TextID"];
		if (!array_key_exists($captionTextID, $langTextArray))
		{
			echo "Language not supported for this page";
			return;
		}
		
		$captionText = $langTextArray[$captionTextID];
		
		print("<p><img src=\"" . $photoUrl . "\"></img></p>");
		print("<p class=\"center\">" . $captionText . "</p>");
		//echo $languageID;
	}
	
	$crResult->close();
?>
</p>
