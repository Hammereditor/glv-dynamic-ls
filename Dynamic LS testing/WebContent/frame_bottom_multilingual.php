
</div>
<div id="wb_CssMenu2" style="position:absolute;width:200px;height:260px;z-index:16;background-color:#DDD; padding-left: 10px;">
	<h4 class="left_sidebar">Languages</h4><br>
	<br>
	
	<script type="text/javascript">
		var locales = [];
		<?php
			include "utils.php";
			$laureateID = $_GET["laureateID"];
			$textIDtoCheck = $_GET["textIDtoCheck"];
			$supportedLanguages = getSupportedLanguages_laureate($laureateID, $textIDtoCheck);
			//var_dump($supportedLanguages);
			
			for ($l = 0; $l < count($supportedLanguages); $l++)
			{
				$langInfoArray = $supportedLanguages[$l];
				$langRightText = "{\"id\": \"" . $langInfoArray["LanguageID"] . "\", \"inLanguageName\": \"" . $langInfoArray["InLanguageName"] . "\", " .
				"\"englishName\": \"" . $langInfoArray["EnglishName"] . "\", \"iconURL\": \"" . $langInfoArray["IconURL"] . "\"};";
				printf("locales[" . $l . "] = " . $langRightText);
				
				/*printf("<div>");
					printf("</img src=\"" . $langInfoArray["IconURL"] . "\">");
					printf("<p>" . $langInfoArray["EnglishName"] . " (" . $langInfoArray["InLanguageName"] . ")</p>");
				printf("</div>");*/
			}
		?>
	</script>
	
	<div id="languageContainer">
		<!-- insert language HTML here -->
	</div>
</div>
